---
title: "Lista 8"
author: "Giovana 197908"
date: "29/06/2020"
output: html_document
---

#######################################
============== QUESTÃO 1 ==============
#######################################

####### A)

```{r}
library(splitstackshape)

#MASTIGAÇÃO DE TABACO PELOS ADOLESCENTES E RISCO DE PERCEPÃO

contingencia1 <- matrix(c(70,33,103,202,40,242,318,11,329,590,84,674),byrow=T,nrow=4)
contingencia1
esperado1 <- matrix(ncol=2,nrow=3)
for(i in 1:3){
  for(j in 1:2){
    esperado1[i,j] <- contingencia1[i,3]*contingencia1[4,j]/contingencia1[4,3]
  }
}
esperado1
qp1 <- 0
for(i in 1:3){
  for(j in 1:2){
    qp1 <- qp1 + ((contingencia1[i,j]-esperado1[i,j])^2/esperado1[i,j])
  }
}
qp1
pchisq(qp1,2,lower.tail=FALSE)

# MASTIGAÇÃO E EDUCAÇÃO DOS PAIS

contingencia2 <- matrix(c(92,26,118,255,44,299,243,14,257,590,84,674),byrow=T,nrow=4)
esperado2 <- matrix(ncol=2,nrow=3)
for(i in 1:3){
  for(j in 1:2){
    esperado2[i,j] <- contingencia2[i,3]*contingencia2[4,j]/contingencia2[4,3]
  }
}
esperado2
qp2 <- 0
for(i in 1:3){
  for(j in 1:2){
    qp2 <- qp2 + ((contingencia2[i,j]-esperado2[i,j])^2/esperado2[i,j])
  }
}
qp2
pchisq(qp2,2,lower.tail=FALSE)


# MASTIGAÇÃO E MASTIGAÇÃO DOS PAIS

contingencia3 <- matrix(c(524,63,587,66,21,87,590,84,674),byrow=T,nrow=3)
esperado3 <- matrix(ncol=2,nrow=2)
for(i in 1:2){
  for(j in 1:2){
    esperado3[i,j] <- contingencia3[i,3]*contingencia3[3,j]/contingencia3[3,3]
  }
}
esperado3
qp3 <- 0
for(i in 1:2){
  for(j in 1:2){
    qp3 <- qp3 + ((contingencia3[i,j]-esperado3[i,j])^2/esperado3[i,j])
  }
}
qp3
pchisq(qp3,1,lower.tail=FALSE)
)
```

####### B)

```{r}
r=2
c=2

n1 <- matrix(c(8,11,25,59),byrow=T,nrow=2)
m1 <- matrix(c(6.08,12.91,26.91,57.08),byrow=T,nrow=2)
v1 <- matrixv(n1,m1,r,c)

n2 <- matrix(c(11,33,29,169),byrow=T,nrow=2)
m2 <-  matrix(c(7.27,36.72,32.72,165.27),byrow=T,nrow=2)
v2 <- matrixv(n2,m2,r,c)

n3 <-  matrix(c(2,22,9,296),byrow=T,nrow=2)
m3 <-  matrix(c(0.8,23.19,10.19,294.8),byrow=T,nrow=2)
v3 <- matrixv(n3,m3,r,c)

a <- matrix(c(1,0,-1,0),byrow=T,nrow=1)

n_vector1 <- as.vector(t(n1))
n_vector2 <- as.vector(t(n2))
n_vector3 <- as.vector(t(n3))
m_vector1 <- as.vector(t(m1))
m_vector2 <- as.vector(t(m2))
m_vector3 <- as.vector(t(m3))

termo1<- (n_vector1-m_vector1)%*%t(a) + (n_vector2-m_vector2)%*%t(a) + 
  (n_vector3-m_vector3)%*%t(a)
termo2 <- solve(a%*%v1%*%t(a)+a%*%v2%*%t(a)+a%*%v3%*%t(a))
termo3 <- a%*%(n_vector1-m_vector1) + a%*%(n_vector2-m_vector2) + 
  a%*%(n_vector3-m_vector3)

qemh <- termo1%*%termo2%*%termo3
pchisq(qemh,1,lower.tail=FALSE)
```

####### C)

```{r}
df <- tibble(risco = rep(c("ng","pg","mg"),each = 6),
             educacao = rep(c("menor2g", "2g", "maior2g"),times = 3,each = 2),
             pais = rep(c("nao", "sim"),9),
             nao = c(15,6,29,4,15,1,11,11,93,17,65,5,36,13,104,8,156,1),
             sim = c(12,3,10,4,3,1,7,0,17,8,5,3,4,0,4,1,1,1))
df_tidy <- df %>% pivot_longer(cols = c("nao", "sim")) %>% uncount(value) %>% 
  magrittr::set_colnames(c("risco","educacao", "pais_usam", "usam")) %>%
  mutate(usam = factor(usam, levels = c("nao","sim")))

#cmh_results <- df_tidy %>% group_by(risco, educacao, pais_usam) %>% count(usam) %>% 
#  xtabs(n ~ usam + pais_usam + educacao + risco,.) %>% 
#  CMHtest(overall = T)

df_tidy <- df_tidy %>% mutate(usam = ifelse(usam=="nao",0,1))
df_tidy <- df_tidy %>% mutate(risco = as.factor(risco),
                              educacao = as.factor(educacao),
                              pais_usam = as.factor(pais_usam))
df_tidy$risco <- relevel(df_tidy$risco, ref="ng")
df_tidy$educacao <- relevel(df_tidy$educacao, ref="menor2g")
df_tidy$pais_usam <- relevel(df_tidy$pais_usam, ref="nao")

modelo1c <- glm(usam ~ risco + educacao + pais_usam, df_tidy, family=binomial(link=logit))
summary(modelo1c)
anova(modelo1c,test="Chisq")

modelo1cnovo <- glm(usam ~risco + educacao, df_tidy, family=binomial(link=logit))
summary(modelo1cnovo)
anova(modelo1cnovo,test="Chisq")
```


#######################################
============== QUESTÃO 2 ==============
#######################################

####### A)

```{r}
contingencia4 <- matrix(c(13,164,177,46,195,241,78,180,258,48,107,155,185,646,831),byrow=T,nrow=5)
contingencia4
esperado4 <- matrix(ncol=2,nrow=4)
for(i in 1:4){
  for(j in 1:2){
    esperado4[i,j] <- contingencia4[i,3]*contingencia4[5,j]/contingencia4[5,3]
  }
}
esperado4
qp4 <- 0
for(i in 1:4){
  for(j in 1:2){
    qp4 <- qp4 + ((contingencia4[i,j]-esperado4[i,j])^2/esperado4[i,j])
  }
}
qp4
pchisq(qp4,3,lower.tail=FALSE)

contingencia5 <- matrix(c(115,231,346,70,415,485,185,646,831),byrow=T,nrow=3)
contingencia5
esperado5 <- matrix(ncol=2,nrow=2)
for(i in 1:2){
  for(j in 1:2){
    esperado5[i,j] <- contingencia5[i,3]*contingencia5[3,j]/contingencia5[3,3]
  }
}
esperado5
qp5 <- 0
for(i in 1:2){
  for(j in 1:2){
    qp5 <- qp5 + ((contingencia5[i,j]-esperado5[i,j])^2/esperado5[i,j])
  }
}
qp5
pchisq(qp5,1,lower.tail=FALSE)
```

####### B)

```{r}
uso <- c(rep("m",8),rep("f",8))
faixa <- rep(c(1,2,3,4),each=2,times=2)
leuko <- rep(c(1,0),8)
count <- c(8,21,24,62,47,82,36,66,5,143,22,133,31,98,12,41)

data <- cbind(uso,faixa,leuko,count)
data <- as.data.frame(data)

data <- data %>% mutate(count = as.numeric(as.character(count)))

data <- data %>% expandRows("count")

fitsat <- glm(leuko ~ faixa+uso, data, family=binomial(link="logit"))
summary(fitsat)
anova(fitsat,test="Chisq")
```


#######################################
============== QUESTÃO 3 ==============
#######################################

####### C)

```{r}
regiao <- rep(c("leste","oeste"),each=12)
poluicao <- rep(c("1baixo","2medio","3alto"),each=4,times=2)
sexo <- rep(c("m","f"),each=2,times=6)
resfriados <- rep(c(0,1),times=12)
ano73 <- c(401,70,433,146,234,31,193,86,50,7,47,10,
           270,47,222,78,212,42,186,77,500,88,435,137)
ano75 <- c(396,75,427,152,239,26,204,75,49,8,46,11,
           249,68,221,79,203,51,187,76,473,115,406,166)

q3 <- as.data.frame(cbind(regiao,poluicao,sexo,resfriados,ano73,ano75))
q3 <- q3 %>% mutate(ano73 = as.numeric(as.character(ano73)),
                    ano75 = as.numeric(as.character(ano75)))

q73 <- q3 %>% select(-ano75) %>% expandRows("ano73")
q75 <- q3 %>% select(-ano73) %>% expandRows("ano75")

#ano de 1973 
modelo73 <- glm(resfriados ~ regiao * poluicao * sexo, q73, 
                family=binomial(link="logit"))
summary(modelo73)
anova(modelo73,test="Chisq")

modelo73novo <- glm(resfriados ~ sexo, q73,
                    family=binomial(link="logit"))
summary(modelo73novo)
anova(modelo73novo,test="Chisq")

#ano de 1975
modelo75 <- glm(resfriados ~ regiao * poluicao * sexo, q75, 
                family=binomial(link="logit"))
summary(modelo75)
anova(modelo75,test="Chisq")

modelo75novo <- glm(resfriados ~ sexo * regiao, q75,
                    family=binomial(link="logit"))
summary(modelo75novo)
anova(modelo75novo,test="Chisq")


#os dois anos juntos 
regiao <- rep(c("leste","oeste"),each=18)
poluicao <- rep(c("1baixo","2medio","3alto"),each=6,times=2)
sexo <- rep(c("m","f"),each=3,times=6)
scores <- rep(c(1,0.5,0),12)
count <- c(19,107,345,61,176,342,6,45,214,31,99,149,2,11,44,6,9,42,
           17,81,219,35,87,178,10,73,171,34,85,144,39,125,424,63,177,332)

qq3 <- as.data.frame(cbind(regiao,poluicao,sexo,scores,count))
qq3 <- qq3 %>% mutate(count=as.numeric(as.character(count)))
qq3 <- qq3 %>% expandRows("count")

qq3$scores <- ordered(qq3$scores)


regpolitomica <- vglm(scores~sexo*regiao, qq3, family=cumulative(parallel=T))
summary(regpolitomica)
anova(regpolitomica,test= c("LRT", "none"))
```


####### D)
```{r}
q3df2 = data.frame(reg=factor(rep(c("Leste","Oeste"),each = 24,times=2)),
               nivel = rep(c("1Baixo","2Médio","3Alto"),each = 8, times = 4),
               sexo = factor(rep(c("masc","fem"),each = 4, times = 12)),
               ano= factor(rep(c("73", "75"), each = 48)),
               resfriados=c(rep(c(1,0),each = 2, times=12),rep(c(1,0), times=24)),
               Freq=rep(c(19,51,56,345,61,85,91,342,6,25,20,214,31,55,44,149,
                      2,5,6,44,6,4,5,42,17,30,51,219,35,43,44,178,
                      10,32,41,171,34,43,42,144,39,49,76,424,63,74,103,332)))

q3dgeral = q3df2 %>% expandRows("Freq")
m3cgeral = glm(resfriados~ reg+sexoano, family=binomial(link="logit"), data=q3dgeral)
summary(m3cgeral)
anova(m3cgeral,test="Chisq")
```


#######################################
============== QUESTÃO 4 ==============
#######################################

```{r}
idade <- rep(c(rep("1menorque25",4),
               rep("2entre25e44",8),
               rep("3entre45e59",4)),7)
sexo <- rep(c(rep(c("m","f"),each=2),
              rep(c("m","f"),each=4),
              rep(c("m","f"),each=2)),7)
filhos <- c("1um","2maisqueum",
            "1um","2maisqueum",
            "1um","2maisqueum","2maisqueum","2maisqueum",
            "1um","2maisqueum","2maisqueum","2maisqueum",
            "1um","2maisqueum",
            "2maisqueum","1um")
novo <- c(rep("1menorque5",6),"2entre5e9","3maisque9",
          rep("1menorque5",2),"2entre5e9",rep("3maisque9",2),
          "2entre5e9",rep("3maisque9",2))
pediatra <- c("0","1","2","3","4a6","7a9","10a30")
count <- c(2,7,1,2,4,1,9,
           3,0,0,0,6,2,11,
           2,1,1,3,6,7,10,
           0,2,2,1,2,6,14,
           1,2,2,6,8,4,18,
           8,5,5,9,24,30,131,
           9,16,8,11,26,5,28,
           16,4,2,6,3,4,6,
           2,0,2,1,6,3,8,
           9,1,3,9,13,11,41,
           8,6,4,8,7,4,18,
           9,1,2,1,2,1,3,
           31,3,3,4,3,3,1,
           5,5,7,3,8,1,4,
           32,4,6,2,6,5,3,
           32,1,2,1,1,0,2)
data <- as.data.frame(cbind(idade,sexo,filhos,novo,pediatra,count))
data <- data %>% mutate(count = as.numeric(as.character(count)))
dataw <- data %>% expandRows("count")
dataw$pediatra <- ordered(dataw$pediatra)


politomica4 <- vglm(pediatra~novo, dataw, family=cumulative(parallel=T))
summary(politomica4)
anova(politomica4,test= c("LRT", "none"))

dataw %>% select(novo, pediatra) %>% group_by(novo) %>% summarise(zero = sum(pediatra=="0"),
                                                                  um = sum(pediatra=="1"),
                                                                  dois = sum(pediatra=="2"),
                                                                  tres = sum(pediatra=="3"),
                                                                  quatroaseis = sum(pediatra=="4a6"),
                                                                  seteanove = sum(pediatra=="7a9"),
                                                                  dezatrinta = sum(pediatra=="10a30"))


```


######################################################
============== QUESTÃO 5 (7 da lista 6) ==============
######################################################

```{r}
cidade <- rep(c("h","k","v"),each=5)
idade <- rep(c(1,2,3,4,5),times=3)
cancer <- c(13,6,15,10,12,
            4,8,7,11,9,
            5,7,10,14,8)
pop <- c(2879,1083,923,834,634,
         3142,1050,895,102,535,
         2520,878,839,631,539)
q7 <- as.data.frame(cbind(cidade,idade,cancer,pop))
q7 <- q7 %>% mutate(cancer=as.numeric(as.character(cancer)),
                    pop=as.numeric(as.character(pop)))
q7 <- q7 %>% mutate(pormil = cancer*1000,
                    lnpop = log(pop))

modelopoi <- glm(pormil ~ cidade*idade, q7, family="poisson", offset=lnpop)
summary(modelopoi)
anova(modelopoi,test="Chisq")
```

```{r}
cidade <- c(rep("Horsens",5),rep("Kolding",5),rep("Vejle",5))
grupos <- c(rep(c("grupo1","grupo2","grupo3","grupo4","grupo5"),3))
casos <- c(13,6,15,10,12,4,8,7,11,9,5,7,10,14,8)
habitantes <- c(2879, 1083, 923, 834, 634,  3142, 1050, 895, 702, 535, 2520, 878, 839, 631, 539)

dados7 <- data.frame(cidade, grupos, casos, habitantes)

dados7_teste <- dados7 %>%
  mutate(casos_mil = casos*1000,
         log_hab = log(habitantes))
  
model7 <- glm(casos_mil ~ cidade + grupos+offset(log_hab), family = poisson(link = "log"), data = dados7_teste)
summary(model7)
model7$deviance/model7$df.residual

#podemos observar que o modelo possui um valor deviance/gl maior que 1000, sendo um indicativo de superdispersão no modelo. Logo sabemos que podemos estar aplicando o modelo da binomial negativa para poder estar ajustando o modelo e "dininuir" esse valor da deviance/gl.

model7 <- glm.nb(casos_mil ~ grupos + offset(log_hab), data = dados7_teste)
summary(model7)
#obtendo o valor de deviance/gl
model7$deviance/model7$df.residual

#Ajustado o modelo por bionomial negativa, temos um modelo melhor do que o modelo de regressão de poisson. Um indicativo de melhora do modelo é o valor da divisão da deviance/gl, que antes era de 1266.944 e agora é de  1.898206
```


