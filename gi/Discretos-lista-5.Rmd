---
title: "LISTA 6"
author: "Giovana 197908"
date: "08/06/2020"
output: html_document
---

############# questão 1

```{r}
library(tidyverse)
library(splitstackshape)

resid <- c(rep("1rural",4),rep("2urbano",4))
stress <- rep(c("1baixo","2alto"),each=2,times=2)
atitude <- c(0,1,0,1,0,1,0,1)
count <- c(14,58,112,116,162,66,64,8)

data <- as.data.frame(cbind(resid,stress,atitude,count),
                      stringsAsFactors = TRUE)
data$count <- as.numeric(as.character(data$count))
data1 <- data %>% expandRows("count")

modelo1sat <- glm(atitude~resid*stress, data1,x=TRUE, family=binomial(link=logit))
summary(modelo1sat)

#######################b) teste de interaçao stress*resid
#pvalor = 0.692

########################c) baixo nivel de stress contra alto nivel de stress
# 1/beta[3]

eta <- unique(modelo1sat$linear.predictors)
beta <- unname(modelo1sat$coefficients)
sdbeta <- c(0.2978,0.3317,0.3259,0.5178)

#rural baixo = eta[1] = beta[1] 
#rural alto = eta[2] = beta[1]+beta[3]
#urbano baixo = eta[3] = beta[1]+beta[2]
#urbano alto = eta[4] = beta[1]+beta[2]+beta[3]

########################c) baixo nivel de stress contra alto nivel de stress
# 1/beta[3]
icinf <- beta[3] - 1.96*sdbeta[3]
icsup <- beta[3] + 1.96*sdbeta[3]
ic <- c(1/exp(icsup),1/exp(icinf))

########################d) residencia urbana contra residência rural
# beta[2]
icinf <- beta[2] - 1.96*sdbeta[2]
icsup <- beta[2] + 1.96*sdbeta[2]
ic <- c(exp(icinf),exp(icsup))

########################e)
# normalidade dos betas

```


############### questão 2
```{r}
centro <- c(rep(1,4),rep(2,4),rep(3,4))
tratamento <- rep(c("a","b"),each=2,times=3)
resposta <- rep(c("0","1"),6)
count <- c(12,3, 9,6, 11,6, 6,11, 30,6, 20,17)


data2 <- as.data.frame(cbind(centro,tratamento,resposta,count))
data2$count <- as.numeric(as.character(data2$count))

data2 <- data2 %>% expandRows("count")
data2 %>% group_by(centro,tratamento) %>% summarise(zero=sum(resposta=="0"),
                                                   um=sum(resposta=="1"))

modelo2sat <- glm(resposta~centro*tratamento,data2,family=binomial(link=logit))
summary(modelo2sat)

beta <- unname(modelo2sat$coefficients)
sdbeta <- c(0.6455,0.8211,0.7853,0.8333,1.0998,1.0016)

#########################b
icinf <- beta[4] - 2.57*sdbeta[4]
icsup <- beta[4] + 2.57*sdbeta[4]
ic <- c(exp(icinf), exp(icsup))

modelo2 <- glm(resposta~centro+tratamento,data2,family=binomial(link=logit))
summary(modelo2)

# TESTE DO AJUSTE DO MODELO POR DEVIANCE
deviance = modelo2$deviance - modelo2sat$deviance
gl = modelo2$df.residual - modelo2sat$df.residual
p_valor = pchisq(deviance, df = gl, lower.tail = FALSE)

```


################### questão 3
```{r}
centro <- c(rep(1,6),rep(2,6),rep(3,6))
tratamento <- rep(c("a","b"),each=3,times=3)
resposta <- rep(c("ruim","razoavel","muitobom"),6)
count <- c(6,6,3, 2,7,6, 4,7,6, 2,4,11, 11,19,6, 6,12,17)

data3 <- as.data.frame(cbind(centro,tratamento,resposta,count))
data3$count <- as.numeric(as.character(data3$count))

data3 <- data3 %>% expandRows("count")
data3 %>% group_by(centro,tratamento) %>% summarise(ruim=sum(resposta=="ruim"),
                                                   razoavel=sum(resposta=="razoavel"),
                                                   muitobom=sum(resposta=="muitobom"))

data3$tratamento <- relevel(data3$tratamento,ref="b")

modelo3 <- vglm(resposta~centro+tratamento,data3,family=cumulative(parallel=T))
summary(modelo3)
################ intervalo de confiança :
betasa <- unname(coefficients(modelo3))
sdbetasa <- c(0.4096,0.3872,0.481,0.4089,0.3374)

betasb <- unname(coefficients(modelo3))
sdbetasb <- c(0.3859,0.4139,0.481,0.4089,0.3374)

icinf <- betas[5]-1.96*sdbetas[5]
icsup <- betas[5]+1.96*sdbetas[5]
ic <- c(exp(icinf),exp(icsup))

modelo3sat <- vglm(resposta~centro*tratamento, data3, family=cumulative(parallel=T))
summary(modelo3sat)

#TESTE DE AJUSTE DO MODELO POR DEVIANCE
deviance = 273.3269 - 273.3247 #modelo3$deviance - modelo3sat$deviance
gl = 265 -  263 #modelo3$df.residual - modelo3sat$df.residual
p_valor = pchisq(deviance, df = gl, lower.tail = FALSE)
```


########################### questão 4
```{r}

# 65-74
exp(2.28+1.15+0.24+0.45)/(1+exp(2.28+1.15+0.24+0.45))
# 75-84 
exp(2.28-0.15+0.24+0.45)/(1+exp(2.28-0.15+0.24+0.45))
# >84
exp(2.28+0.24+0.45)/(1+exp(2.28+0.24+0.45))

############## b
1/exp(0.52)

icinf <- exp(0.52 - 1.96*0.18)
icsup <- exp(0.52 + 1.96*0.18)
ic <- c(1/icsup, 1/icinf)


############### c
deviance = (32.51 - 41.46) *(-2)
gl = 14
p_valor = pchisq(deviance, df = gl, lower.tail = FALSE)
p_valor
```


########################### questão 5
```{r}
data5 = read.table(header=T,text="
id grupo_idade idade resp
1  1  20  0
2  1  23  0
3  1  24  0
4  1  25  0
5  1  25  1
6  1  26  0
7  1  26  0
8  1  28  0
9  1  28  0
10 1  29  0
11 2  30  0
12 2  30  0
13 2  30  0
14 2  30  0
15 2  30  0
16 2  30  1
17 2  32  0
18 2  32  0
19 2  33  0
20 2  33  0
21 2  34  0
22 2  34  0
23 2  34  1
24 2  34  0
25 2  34  0
26 3  35  0
27 3  35  0
28 3  36  0
29 3  36  1
30 3  36  0
31 3  37  0
32 3  37  1
33 3  37  0
34 3  38  0
35 3  38  0
36 3  39  0
37 3  39  1
38 4  40  0
39 4  40  1
40 4  41  0
41 4  41  0
42 4  42  0
43 4  42  0
44 4  42  0
45 4  42  1
46 4  43  0
47 4  43  0
48 4  43  1
49 4  44  0
50 4  44  0
51 4  44  1
52 4  44  1
53 5  45  0
54 5  45  1
55 5  46  0
56 5  46  1
57 5  47  0
58 5  47  0
59 5  47  1
60 5  48  0
61 5  48  1
62 5  48  1
63 5  49  0
64 5  49  0
65 5  49  1
66 6  50  0
67 6  50  1
68 6  51  0
69 6  52  0
70 6  52  1
71 6  53  1
72 6  53  1
73 6  54  1
74 7  55  0
75 7  55  1
76 7  55  1
77 7  56  1
78 7  56  1
79 7  56  1
80 7  57  0
81 7  57  0
82 7  57  1
83 7  57  1
84 7  57  1
85 7  57  1
86 7  58  0
87 7  58  1
88 7  58  1
89 7  59  1
90 7  59  1
91 8  60  0
92 8  60  1
93 8  61  1
94 8  62  1
95 8  62  1
96 8  63  1
97 8  64  0
98 8  64  1
99 8  65  1
100 8 69  1")

modeloq5 <- glm(resp ~ idade, data=data5, family=binomial(link=logit))
summary(modeloq5)
```

